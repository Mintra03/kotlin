package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ManufacturerDao
import dv.spring.kotlin.entity.Manufacturer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl: ManufacturerService{
    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturer(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }
}