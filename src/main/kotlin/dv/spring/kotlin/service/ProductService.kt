package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Product

interface ProductService {
    fun getProduct(): List<Product>
}