package dv.spring.kotlin.dao

import dv.spring.kotlin.Testclass.Product

interface ProductDao {
    fun getProduct(): List<dv.spring.kotlin.entity.Product>
}