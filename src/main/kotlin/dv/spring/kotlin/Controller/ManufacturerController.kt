package dv.spring.kotlin.Controller

import dv.spring.kotlin.service.ManufacturerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ManufacturerController{
    @Autowired
    lateinit var manufacturerService: ManufacturerService
    @GetMapping("/manufacturer")
    fun getAllManufacturer():ResponseEntity<Any>{
        return ResponseEntity.ok(manufacturerService.getManufacturer());
    }
}