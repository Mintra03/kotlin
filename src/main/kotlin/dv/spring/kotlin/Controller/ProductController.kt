package dv.spring.kotlin.Controller

import dv.spring.kotlin.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController{
    @Autowired
    lateinit var productService: ProductService
    @GetMapping("/product")
    fun getProduct():ResponseEntity<Any>{
        return ResponseEntity.ok(productService.getProduct())
    }
}