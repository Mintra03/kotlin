package dv.spring.kotlin.entity

import javax.validation.constraints.Email

data class Customer (
        override var name: String,
        override var email: String,
        override var userStatus: UserStatus = UserStatus.PENDING
) : User {
    var shippingAddress = mutableListOf<Address>()
    lateinit var billingAddress: Address
    lateinit var defaultAddress: Address
}
