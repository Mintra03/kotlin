package dv.spring.kotlin.entity

data class Address (var homeAddress: String,
                    var subdistrict: String,
                    var district: String,
                    var province: String,
                    var postCode: String)