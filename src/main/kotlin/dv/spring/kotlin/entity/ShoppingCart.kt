package dv.spring.kotlin.entity

data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT) {
    var selectedProduct = mutableListOf<SelectedProduct>()
    lateinit var customer: Customer
    lateinit var shippingAddress: Address
}

