package dv.spring.kotlin.entity

import dv.spring.kotlin.Testclass.Product

data class Manufacturer (var name: String, var telNo: String) {
    var products = mutableListOf<Product>()
//    lateinit var address : Address
}