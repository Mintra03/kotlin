package dv.spring.kotlin.entity

data class Admin (
        override var name: String,
        override var email: String,
        override var userStatus: UserStatus = UserStatus.PENDING
): User