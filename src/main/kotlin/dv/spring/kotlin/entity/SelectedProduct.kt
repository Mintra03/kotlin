package dv.spring.kotlin.entity

data class SelectedProduct (var quantity: Int) {
    var products = mutableListOf<Product>()
}